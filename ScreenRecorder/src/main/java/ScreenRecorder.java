
import java.util.LinkedList;
import java.util.List;

public class ScreenRecorder {

	List<Profile> list = new LinkedList<Profile>();
	private boolean currentProfile = false;
	private boolean startRecorrind = false;
	private int liczbaPowtorzenStartu = 0;
	private int liczbaPowtorzenStopu = 0;

	public void add(Profile profile) {
		System.out.println("Dodano profil: "
				+ list.add(new Profile(profile.getCodec(), profile.getExtension(), profile.getResolution())));
	}

	public void list() {
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Numer profilu: " + i + " " + list.get(i).toString());
		}
	}

	public void stardRecording() {
		if (currentProfile == true && liczbaPowtorzenStartu == 0) {
			System.out.println("Rozpoczynam nagrywanie");
			startRecorrind = true;
			liczbaPowtorzenStartu++;
		}
		if (liczbaPowtorzenStartu > 1) {
			System.out.println("Nagranie ju� rozpocz�to");
		}
		if (currentProfile == false) {
			System.out.println("Nie ustawiono profilu");
		}
	}

	public void stopRecording() {
		if (startRecorrind == true && liczbaPowtorzenStopu == 0) {
			System.out.println("Zako�czam nagrywanie");
			liczbaPowtorzenStopu++;
		}
		if (liczbaPowtorzenStopu > 1) {
			System.out.println("Nie mo�na zako�czy� zako�czonego nagrywania");
			liczbaPowtorzenStartu = 0;
		}
		if (startRecorrind == false) {
			System.out.println("Nie rozpocz�to nagrywania");
		}
	}

	public void setProfile(int i) {
		try {
			System.out.println("Wybrano profil:" + list.get(i));
			currentProfile = true;
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Nie ma takiego profilu");
		}

	}

}
